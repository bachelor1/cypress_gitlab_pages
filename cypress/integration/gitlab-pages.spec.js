/************************************************************************
* All tests below are ran inside browser "Electron 94" with Test runner
* @see https://docs.cypress.io/guides/core-concepts/test-runner
* Cypress tests can be configured to run on the followin browsers:
* @see https://docs.cypress.io/guides/guides/launching-browsers#Browsers
* @author Nicholas Sellevåg
* @date 03.05.2022
************************************************************************/


/***********************************************************
 * Test wether the navigation menu part of bachelor thesis's 
 * gitlab template can be opened or not.
 * 
 * @returns true when openable, false when not. 
 * @author Nicholas Bodvin Sellevåg
 * @date 03.05.2022
 **********************************************************/
describe('Check navigation menu becomes visible', () => {
	it('Opens hamburger menu', () => {
		cy.visit('replace-me')
		cy.get('.hamburger').click()
		cy.get('.hamburger-content').should('be.visible')
	})
})

/***********************************************************
 * Retrieves all anchor tags in DOM of bachelor Thesis's
 * index page. 
 * Filters away anchor tags that are for mails.
 * Validates remaining links are valid, if link returns
 * web error codes such as 404 it is considered as not
 * working.
 * 
 * @returns false when link receives web error codes 
 * @author Nicholas Bodvin Sellevåg
 * @date 03.05.2022
 **********************************************************/
describe('Check for broken links', () => {
	it('Clicks on all links in index page', () => {
		cy.visit('replace-me')
		cy.get("a:not([href*='mailto:'])").each(page => {
			cy.request(page.prop('href'))
		  })
	})
})

/***********************************************************
 * Retrieves element with a class of "contact list".
 * Validates that class has two child elements.
 * Test serves a demonstrative purpose, wether there are
 * two entries or more does not depict that website is 
 * working correctly. In future, such tests could be 
 * updated with validating the textual content of a course's
 * contact list.  
 * 
 * @returns true when two child elements in "contact list". 
 * @author Nicholas Bodvin Sellevåg
 * @date 03.05.2022
 **********************************************************/
describe('Check contact info', () => {
	it('Validates that course responsible is updatd', () => {
		cy.visit('replace-me')
		cy.get(".contact-list >li").should('have.length', 2)
	})
})