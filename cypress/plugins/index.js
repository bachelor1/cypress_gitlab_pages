/// <reference types="cypress" />

/*******************************************************************************
 * Function is called whenever a project is opened or re-opened.
 * For example, when a project's configuration changes the project will re-open.
 * @type {Cypress.PluginConfig}
 * @author Nicholas Bodvin Sellevåg
 * @date 03.05.2022
 ******************************************************************************/

module.exports = (on, config) => {
  require('cypress-mochawesome-reporter/plugin')(on);
}

