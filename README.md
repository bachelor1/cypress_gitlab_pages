# cypress_gitlab_pages
Creates a docker container throug Gitlab CI/CD with automated testing for websites.
As of now used in relation to "Git da gitt" bachelor thesis: [README](https://gitlab.stud.idi.ntnu.no/bachelor1/Website/-/tree/master)

Uses Cypress to enact javascripted testing on a given website.
Cypress is an end-to-end testing framework for web test automation: [Docuemtnation](https://www.cypress.io/)
