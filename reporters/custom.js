/***********************************************************
 * Configurations of cypress reporters that generate html
 * report is configured below.
 * For example: 
 * Choice of "reporter"
 * Type of report "html"
 *  
 * @author Nicholas Bodvin Sellevåg
 * @date 03.05.2022
 **********************************************************/
{
    "reporter": "cypress-mochawesome-reporter",
    "reporterOptions": {
      "toConsole": true
      "html": true
    }
  }
